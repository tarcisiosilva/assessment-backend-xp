<?php
class ProductController  extends Controller
{
   
    public function ListProduct()
    {       
         $product = Product::all() ;         
         return $this->view(basename.'/templates/product',['dados'=> $product]);
       
    }
    
    public function criar()   {   
        
        $category = Category::all() ;
        return $this->view(basename.'/templates/addProduct',['categories' => $category]);
    }
    
    public function editar($dados)
    {   
        $category = Category::all() ;
        $id      = (int) $dados['id'];
        $dados = Product::find($id);      
        return $this->view(basename.'/templates/addProduct', ['data' => $dados,'categories' => $category]);
    } 
   
    public function salvar()
    {  
        $product  = new Product;      
        $product->name_product    = "'".$_POST['name_product']."'";
        $product->category        = "'(".implode(',',$_POST['category']).")'";  
        $product->price           =  $replaced_number = str_replace(array('R$ ','.',','), array('','','.'), $_POST['price']);  
        $product->sku             = $_POST['sku'];   
        $product->quantity        = $_POST['quantity'];
        $product->description     = "'".$_POST['description']."'";   
        $product->fileimage       = '"'.$_FILES['image_file']['name'].'"';
             
        try {
            if (move_uploaded_file($_FILES["image_file"]["tmp_name"],basename."/assets/images/product/".$_FILES['image_file']['name']
                       )){
                if ($product->save()) {
                    header('Location:/?controller=ProductController&method=ListProduct');
                    exit;
                }else{
                    throw new Exception('Erro ao Cadastrar o Produto.');
                }
            }
        } catch (Exception  $e) {
            $e->getMessage();
        }   
    }
 
    public function atualizar($dados)    {
        
       
        $id                = (int) $dados['id'];
        $product           = Product::find($id);
        $product->name_product     = "'".$_POST['name_product']."'";
        $product->category        = "'(".implode(',',$_POST['category']).")'";  
        $product->price           =  $replaced_number = str_replace(array('R$ ','.',','), array('','','.'), $_POST['price']);  
        $product->sku             = $_POST['sku'];   
        $product->quantity        = $_POST['quantity'];
        $product->description     = "'".$_POST['description']."'";             
       
        if($_FILES['image_file']['name']){
            $product->fileimage       = '"'.$_FILES['image_file']['name'].'"';             
            try {
                 if (move_uploaded_file($_FILES["image_file"]["tmp_name"],basename."/assets/images/product/".$_FILES['image_file']['name']
                       )){
                 if ($product->save()) {
                    header('Location:/?controller=ProductController&method=ListProduct');
                    exit;
                 }else{
                    throw new Exception('Erro ao Cadastrar o Produto.');
                 }
            }
            } catch (Exception  $e) {
                $e->getMessage();
            } 
        }else{
            $product->fileimage        = '"'.$_POST['img_file'].'"';  
            if ($product->save()) {
                header('Location:/?controller=ProductController&method=ListProduct');
                exit;
             }else{
                throw new Exception('Erro ao Cadastrar o Produto.');
             }
        }

    }
 
  
    public function excluir($dados) 
    {
        $id      = (int) $dados['id'];
        if($product = Product::delete($id)){
        header('Location:/?controller=ProductController&method=ListProduct');
        exit;
        }
    }

    
    public function findProduct($dados)
    {    
        
         $product = Product::findProduct($dados) ;
         //return $product;
         echo $product;
       
    }



}

?>