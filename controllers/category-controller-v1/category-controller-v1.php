<?php
class CategoryController  extends Controller
{
     /**
     * List Category
     */
    public function ListCategory()
    {
        $category = category::all();
        return $this->view(basename.'/templates/categories',['category'=> $category]);
    }
 
    /**
     * Mostrar formulario para criar um novo contato
     */
    public function criar()
    {
        return $this->view(basename.'/templates/addCategory');
    }
 
    /**
     * Mostrar formulário para editar um contato
     */
    public function editar($dados)
    {
        $id      = (int) $dados['id'];
        $Category = Category::find($id);
      
         return $this->view(basename.'/templates/addCategory', ['data' => $Category]);
    }
 
    /**
     * Salvar o contato submetido pelo formulário
     */
    public function salvar()
    {
        $Category           = new Category;
        $Category->name_category     = '"'.$_POST['name_category'].'"';
        $Category->code              = $_POST['code'];    
        if ($Category->save()) {
            header('Location:/?controller=CategoryController&method=ListCategory');
            exit;
        }else{
            echo 'erro';
        }
    }
 
    /**
     * Atualizar o contato conforme dados submetidos
     */
    public function atualizar($dados)
    {
        $id                = (int) $dados['id'];
        $Category           = Category::find($id);
        $Category->name_category     = '"'.$_POST['name_category'].'"';
        $Category->code              = $_POST['code'];
        $Category->save(); 
        if ($Category->save()) {
            header('Location:/?controller=CategoryController&method=ListCategory');
            exit;
        }else{
            echo 'erro';
        }
    }
 
    /**
     * Apagar um contato conforme o id informado
     */
    public function excluir($dados)    {
        $id      = (int) $dados['id'];        
        if ( $Category = Category::delete($id)) {
            header('Location:/?controller=CategoryController&method=ListCategory');
            exit;
        }else{
            echo 'erro';
        }
    }
}

?>