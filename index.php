<?php
error_reporting(E_ALL);
ini_set('display_errors', false);
require_once( __DIR__ . '/config/settings.php');
require_once( __DIR__ . '/config/helpers-settings.php');
require_once( __DIR__ . '/controllers/config.php');
require_once( __DIR__ . '/modelo/config.php');
?>
<html ⚡>
    <header>
        <meta charset="utf-8">       
        <link  rel="stylesheet" type="text/css"  media="all" href="assets/css/style.css" />
        <script src='https://code.jquery.com/jquery-3.4.1.min.js'></script> 
        <script src="https://rawgit.com/RobinHerbots/Inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
        <script src="assets/js/app.js"></script>
  
         
    </header>
    <body>
        <?php
        if ($_GET) {
            $controller = isset($_GET['controller']) ? ((class_exists($_GET['controller'])) ? new $_GET['controller'] : NULL ) : null;
            $method     = isset($_GET['method']) ? $_GET['method'] : null;                             
            if ($controller && $method) {
                if (method_exists($controller, $method)) {
                    $parameters = $_GET;
                    unset($parameters['controller']);
                    unset($parameters['method']);
                    call_user_func(array($controller, $method), $parameters);
                } else {
                    echo "Método não encontrado!";
                }
            } else {
                echo "Controller não encontrado!";
            }
        } else {
            call_user_func(array("Router", 'routerHome'));
        }    
        ?>
    </body>
</html>