<?php

class Category {
private $atributos;
 
    public function __construct()
    {
 
    }
 
    public function __set(string $atributo, $valor)
    {
        $this->atributos[$atributo] = $valor;
        return $this;
    }
 
    public function __get(string $atributo)
    {
        return $this->atributos[$atributo];
    }
 
    public function __isset($atributo)
    {
        return isset($this->atributos[$atributo]);
    }


    /* Inserir Categorias  /  update */

    public function save() {
     
        $colunas = $this->atributos;      
        if (!isset($this->id)) {
            $query = "INSERT INTO categorias (".
                implode(', ', array_keys($colunas)).
                ") VALUES (".
                implode(', ', array_values($colunas)).");";
        } else {
            foreach ($colunas as $key => $value) {
                if ($key !== 'id' && $key!=='crated_date') {
                    $definir[] = "{$key}={$value}";
                }
            }
            $query = "UPDATE categorias SET ".implode(', ', $definir)." WHERE id='{$this->id}';";
        }     
      
              
        if ($conexao = Conexao::getInstance()) {           
            $stmt = $conexao->prepare($query);
             if ($stmt->execute()) {
                return true;
            }
        }
        return false;
    }

    public static function delete($id)
    {
        $conexao = Conexao::getInstance();
        if ($conexao->exec("DELETE FROM categorias WHERE id='{$id}';")) {
            return true;
        }
        return false;
    }

    public static function all()
    {
        $conexao = Conexao::getInstance();
        $stmt    = $conexao->prepare("SELECT * FROM categorias;");
        $result  = array();
        if ($stmt->execute()) {
            while ($rs = $stmt->fetchObject(Category::class)) {
                $result[] = $rs;
            }
        }
        if (count($result) > 0) {
            return $result;
        }
        return false;
    }
    public static function find($id)
    {
        $conexao = Conexao::getInstance();
        $stmt    = $conexao->prepare("SELECT * FROM categorias WHERE id='{$id}';");
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                $resultado = $stmt->fetchObject('Category');
                if ($resultado) {
                    return $resultado;
                }
            }
        }
        return false;
    }
    private function escapar($dados)
    {
        if (is_string($dados) & !empty($dados)) {
            return "'".addslashes($dados)."'";
        } elseif (is_bool($dados)) {
            return $dados ? 'TRUE' : 'FALSE';
        } elseif ($dados !== '') {
            return $dados;
        } else {
            return 'NULL';
        }
    }

    /**
     * Verifica se dados são próprios para ser salvos
     * @param array $dados
     * @return array
     */
    private function preparar($dados)
    {
        $resultado = array();
        foreach ($dados as $k => $v) {
            if (is_scalar($v)) {
                $resultado[$k] = $this->escapar($v);
            }
        }
        return $resultado;
    }
}
?>