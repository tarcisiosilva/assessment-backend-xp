<?php
class Product {

private $atributos;
 
    public function __construct()
    {
 
    }
 
    public function __set(string $atributo, $valor)
    {
        $this->atributos[$atributo] = $valor;
        return $this;
    }
 
    public function __get(string $atributo)
    {
        return $this->atributos[$atributo];
    }
 
    public function __isset($atributo)
    {
        return isset($this->atributos[$atributo]);
    }

    public function save() {     
        $colunas = $this->atributos;      
        if (!isset($this->id)) {
            $query = "INSERT INTO products (".
                implode(', ', array_keys($colunas)).
                ") VALUES (".
                implode(', ', array_values($colunas)).");";
        } else {
            foreach ($colunas as $key => $value) {
                if ($key !== 'id' && $key!=='created_date') {
                    $definir[] = "{$key}={$value}";
                }
            }
            $query = "UPDATE products SET ".implode(', ', $definir)." WHERE id='{$this->id}';";
        }           
        if ($conexao = Conexao::getInstance()) {           
            $stmt = $conexao->prepare($query);
             if ($stmt->execute()) {
                return true;
            }
        }
        return false;
    }

    public static function delete($id)
    {
        $conexao = Conexao::getInstance();
        if ($conexao->exec("DELETE FROM products WHERE id='{$id}';")) {
            return true;
        }
        return false;
    }

    public static function all()
    {
        $conexao = Conexao::getInstance();
        $stmt    = $conexao->prepare("SELECT * FROM products;");
        
        $result  = array();
        if ($stmt->execute()) {
            while ($rs = $stmt->fetchObject(Product::class)) {
                $result[] = $rs;
            }
        }    
        if (count($result) > 0) {
            return $result;
        }
        return false;
    }

    public static function find($id)
    {
        $conexao = Conexao::getInstance();
        $stmt    = $conexao->prepare("SELECT * FROM products WHERE id='{$id}';");
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                $resultado = $stmt->fetchObject('Product');
                if ($resultado) {
                    return $resultado;
                }
            }
        }
        return false;
    }

    public static function findProduct($id)
    {
        $conexao = Conexao::getInstance();
        $stmt    = $conexao->prepare("SELECT * FROM categorias WHERE id iN ".$id.";");
              if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                //$resultado = $stmt->fetchObject('Product');
                $resultado = $stmt->fetchAll();
                 if ($resultado) {
                     foreach($resultado as $v => $k){
                        $resultados[] = $k['name_category'];                        
                     }   
                     $categorias = implode(",", $resultados);
        
                     return $categorias;
                }
            }
        }
        return false;
    }

    
   
}
?>