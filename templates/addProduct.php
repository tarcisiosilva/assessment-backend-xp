
<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Add Product</title>
  <meta charset="utf-8">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script></head>
  <!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="<?php echo images; ?>/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="/"><img src="<?php echo images; ?>/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
    <li><a href="?controller=CategoryController&method=ListCategory" class="link-menu">Categorias</a></li>
      <li><a href="?controller=ProductController&method=ListProduct" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar> 
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="/" class="link-logo"><img src="<?php echo images; ?>/go-logo.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<!-- Header -->
  <!-- Main Content -->
  <main class="content"> 
    <h1 class="title new-item">Novo Produto</h1>   
 
    <form action='?controller=ProductController&<?php echo isset($data->id) ? "method=atualizar&id={$data->id}" : "method=salvar"; ?>' method='post'  enctype="multipart/form-data" >
      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" name='sku' id="sku" class="input-text" required="required"  value="<?php
                echo isset($data->sku) ? $data->sku : null;
                ?>"/>
      </div>
      <div class="input-field">
        <label for="name" class="label">Nome Produto</label>
        <input type="text" id="name" class="input-text" name='name_product' required="required"  value="<?php
                echo isset($data->name_product) ? $data->name_product : null;
                ?>"/>
      </div>
      <div class="input-field">
        <label for="name" class="label">Imagem Produto</label>        
        <input type="file" id="file" class="input-text" name='image_file' accept="image/png, image/jpeg" 
        <?php echo ($img = isset($data->fileimage) ?  $data->fileimage: null) == NULL ? 'required="required"'  :  NULL?>/>
        <input type="hidden" name='img_file'  value="<?php echo isset($data->fileimage) ?  $data->fileimage: null;?>" />
        <?php if($data->fileimage) { ?>
                <img src="<?php echo images_products. $data->fileimage; ?>" width="150px" height="100px" />
        <?php } ?>
        </td>
        </tr>
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" id="price" class="input-text" name='price' required="required"  value="<?php
                echo isset($data->price) ? $data->price : null;
                ?>"/> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" id="quantity" class="input-text" name='quantity'required="required"  value="<?php
                echo isset($data->quantity) ? $data->quantity : null;
                ?>"/> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categories</label>  

        <select multiple="multiple" id="category" class="input-text" name='category[]' required="required" > 
         <?php 
                foreach ($categories as $c ){
                  ?>
                  <option <?php echo (strpos($data->category,$c->id) ? "selected" : ''); ?> value='<?php echo $c->id;?>'>
                  <?php echo $c->name_category?>
                  </option>
                  <?php
                 }
             ?>   
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Descrição</label>
        <textarea id="description" name="description" class="input-text" >
            <?php
                echo isset($data->description) ? $data->description : null;
                ?>
          </textarea>
      </div>
      <div class="actions-form">
        <a href="?controller=ProductController&method=ListProduct" class="action back">Voltar</a>
        <input class="btn-submit btn-action" type="submit" value="Salvar" />
      </div>
      
    </form>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<footer>
	<div class="footer-image">
	  <img src="<?php echo images; ?>/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
	</div>
	<div class="email-content">
	  <span>go@jumpers.com.br</span>
	</div>
</footer>
 <!-- Footer --></body>
</html>
